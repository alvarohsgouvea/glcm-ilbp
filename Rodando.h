#include <stdio.h>
#include <math.h>
#include "ILBP.h" 
#include "GLCM.h"

void Printa_Bonito(int A, int G){

	printf("Asfalto: %.2d%%\n", 100*A/25);
	printf("Grama:   ");
	printf("%.2d%%\n", 100*G/25);
}

float Euclidiana(float *Imagem, float *Padrao){
	float Distancia = 0;

	for(int i = 0; i < 536; i++){
		Distancia += (*(Imagem+i) - *(Padrao+i)) * (*(Imagem+i) - *(Padrao+i));
	}

	Distancia = sqrt(Distancia);

	return Distancia;
}

void Casos_Treino(float *Padrao_Textura_A, float *Padrao_Textura_G, int *Imagem_A, int *Imagem_G){
	char tipo[100] = {"DataSet//asphalt//asphalt_"}, nome[100];
	int matriz[1025][1025], N, i;
	float *texturaImag;

	srand(time(NULL));

	for(i = 1; i <= 50; i++){

		system("clear");
		printf("Treinando:\n");

		if(i < 26){
			do{
				N = rand()%50;
			}while(Imagem_A[N] == 1);
			Imagem_A[N]++;
		}else{
			do{
				N = rand()%50;
			}while(Imagem_G[N] == 1);
			Imagem_G[N]++;	
		}
		N++;

		if(i > 25){
			strcpy(tipo, "DataSet//grass//grass_");
			Printa_Bonito(25, i-25);
		}
		else{
			Printa_Bonito(i, 0);
		}

		snprintf(nome, 100, "%s%.2d.txt", tipo, N);

		texturaImag = (float *)calloc(536, sizeof(float));

		if(texturaImag == NULL){
			printf("Morreu x_x\n");
			exit(1);
		}
		ILBP(nome, texturaImag, &matriz[0][0]);
		GLCM(texturaImag, &matriz[0][0]);
		if(i < 26){
			for(int i = 0; i < 536; i++){
				*(Padrao_Textura_A+i) += *(texturaImag+i)/25.00;
			}
		}else{
			for(int i = 0; i < 536; i++){
				*(Padrao_Textura_G+i) += *(texturaImag+i)/25.00;
			}
		}
	free(texturaImag);
	}
}

void Casos_Teste(float *Padrao_Textura_A, float *Padrao_Textura_G, int *Imagem_A, int *Imagem_G){
	char tipo[100] = {"DataSet//asphalt//asphalt_"}, nome[100];
	int  matriz[1025][1025], N, i;
	float Grama, Asfalto, *texturaImag;
	float Certo = 0, Falso_Aceito = 0, Falso_Rejeitado = 0;

	srand(time(NULL));

	for(int i = 1; i <= 50; i++){

		system("clear");
		printf("Treinando:\nAsfalto: 100%%\nGrama:   100%%\n\nTestando:\n");
		printf("Asfalto: ");
		if(i < 26){
			printf("%.2d%%\nGrama:   %.2d%%\n", 100*i/25, 0);
		}
		else{
			printf("100%%\nGrama:   %.2d%%\n", 100*(i-25)/25);
		}

		if(i < 26){
			do{
				N = rand()%50;
			}while(Imagem_A[N] == 1);
			Imagem_A[N]++;
		}else{
			do{
				N = rand()%50;
			}while(Imagem_G[N] == 1);
			Imagem_G[N]++;	
		}
		N++;

		if(25 < i)
			strcpy(tipo, "DataSet//grass//grass_");

		snprintf(nome, 100, "%s%.2d.txt", tipo, N);

		texturaImag = (float *)calloc(536, sizeof(float));

		if(texturaImag == NULL){
			printf("Morreu x_x\n");
			exit(1);
		}
		ILBP(nome, texturaImag, &matriz[0][0]);
		GLCM(texturaImag, &matriz[0][0]);

		Asfalto = Euclidiana(texturaImag, Padrao_Textura_A);
		Grama = Euclidiana(texturaImag, Padrao_Textura_G);	

		if(i < 26 && Grama > Asfalto)
			Certo++;
		if(i > 25 && Asfalto > Grama)
			Certo++;
		if(i < 26 && Asfalto > Grama)
			Falso_Aceito++;
		if(i > 25 && Grama > Asfalto)
			Falso_Rejeitado++;
	}
	printf("\nResultado:\nCerto: %.2f%%\nFalso Aceito: %.2f%%\nFalso Rejeitado %.2f%%\n", 100*Certo/50.00, 100*Falso_Aceito/50.00, 100*Falso_Rejeitado/50.00);
}