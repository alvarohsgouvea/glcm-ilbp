#include <stdio.h>
int rotaciona(int *binario){
	int rotacao[9], menor, valor;

	for(int i = 0; i < 9; i++){
		valor = 0;
		for(int j = 0; j < 9; j++){
			if(j+i < 9)
				rotacao[j] = *(binario+j+i);
			else
				rotacao[j] = *(binario+j+i-9);
		}
		valor = rotacao[0]*256+rotacao[1]*128+rotacao[2]*64+rotacao[3]*32+rotacao[4]*16+rotacao[5]*8+rotacao[6]*4+rotacao[7]*2+rotacao[8];
		if(i == 0)
			menor = valor;
		else if(menor > valor)
				menor = valor;
	}
	return menor;
}

void ILBP (char tipo[], float *text, int *matriz) {
	int binario[9], ContBin;
	float media = 0.0;
	FILE *Image;

	Image = fopen(tipo, "r");
	for(int i = 0; i < 1025; i++){
		for(int j = 0; j < 1025; j++){
			fscanf(Image, "%d;", matriz+i*1025+j);
		}
	}
	fclose(Image);

	for(int i = 1; i < 1024; i++){
		for(int j = 1; j < 1024; j++){
			media = 0;
			for(int lin = i-1; lin < i+2; lin++){
				for(int col = j-1; col < j+2; col++){
					media = media+*(matriz+lin*1025+col);
				}
			}
			media = media/9.0;
			ContBin = 0;
			for(int lin = i-1; lin < i+2; lin++){
				for(int col = j-1; col < j+2; col++){
					if(*(matriz+lin*1025+col) >= media)
						binario[ContBin] = 1;
					else{
						binario[ContBin] = 0;
					}
					ContBin++;
				}
			}
		*(text + rotaciona(binario))+=1;
		} 
	}
}
