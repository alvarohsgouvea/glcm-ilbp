/*
Álvaro Henrique de Sousa Gouvea - 18/0012002
Fernando Vargas Teotônio de Oliveira - 18/0016491
Luís Guilherme Gaboardi Lins - 18/0022962
*/
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "Rodando.h"


int main() {
	float *Padrao_Textura_A, *Padrao_Textura_G; 
	int *Imagem_A, *Imagem_G;

	Padrao_Textura_G = (float *)calloc(536, sizeof(float));
	Padrao_Textura_A = (float *)calloc(536, sizeof(float));
	Imagem_A = (int *)calloc(50, sizeof(int));
	Imagem_G = (int *)calloc(50, sizeof(int));
	if(Padrao_Textura_G == NULL || Padrao_Textura_A == NULL || Imagem_A == NULL || Imagem_G == NULL){
		printf("Morreu x_x\n");
		exit(1);
	}

	Casos_Treino(Padrao_Textura_A, Padrao_Textura_G, Imagem_A, Imagem_G);
	Casos_Teste(Padrao_Textura_A, Padrao_Textura_G, Imagem_A, Imagem_G);

	free(Padrao_Textura_G);
	free(Padrao_Textura_A);
	free(Imagem_G);
	free(Imagem_A);
	return 0;
}
