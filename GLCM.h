#include <stdio.h>
#include <math.h>

void Normaliza(float *Padrao){
	float min, max;

	min = *Padrao;
	max = *Padrao;

	for(int i = 0; i < 536; i++){
		if(*(Padrao+i)>max)
			max = *(Padrao+i);

		if(*(Padrao+i)<min)
			min = *(Padrao+i);
	}
	for(int i = 0; i < 536; i++){
		*(Padrao+i) = (*(Padrao+i) - min)/(max - min);
	}
}

void GLCM(float *Padrao, int *matrix){
	float contraste[4] = {0, 0, 0, 0}, energia[4] = {0, 0, 0, 0}, homogeneidade[4] = {0, 0, 0, 0};
	int **GreyContrast, x, y, calc, k = 0;

	GreyContrast = calloc(256, sizeof(int *)); 
	for(int i = 0; i < 256; i++)
		GreyContrast[i] = calloc(256, sizeof(int));

	for(int i = 0; i < 1024; i++){
		for(int j = 0; j < 1025; j++){
			x = *(matrix+(i+1)*1025+j); 
			y = *(matrix+i*1025+j); 

			GreyContrast[x][y]++;
		}
	}
	for(int i = 0; i < 256; i++){
		for(int j = 0; j < 256; j++){
			calc = GreyContrast[i][j];
			contraste[0] += fabs(i - j)*(calc*calc);
			energia[0] += (calc*calc);
			homogeneidade[0] += calc/(fabs(i - j)+1);
		}
	}
	free(GreyContrast);

	GreyContrast = calloc(256, sizeof(int *)); 
	for(int i = 0; i < 256; i++){
		GreyContrast[i] = calloc(256, sizeof(int)); 
	}


	for(int i = 0; i < 1024; i++){
		for(int j = 0; j < 1024; j++){
			x = *(matrix+i*1025+j);
			y = *(matrix+(i+1)*1025+(j+1));

			GreyContrast[x][y]++;
		}
	}
	for(int i = 0; i < 256; i++){
		for(int j = 0; j < 256; j++){
			calc = GreyContrast[i][j];
			contraste[1] += fabs(i - j)*(calc*calc);
			energia[1] += (calc*calc);
			homogeneidade[1] += calc/(fabs(i - j)+1);
		}
	}
	free(GreyContrast);

	GreyContrast = calloc(256, sizeof(int *)); 
	for(int i = 0; i < 256; i++){
		GreyContrast[i] = calloc(256, sizeof(int)); 
	}


	for(int i = 0; i < 1025; i++){
		for(int j = 0; j < 1024; j++){
			x = *(matrix+i*1025+j);
			y = *(matrix+i*1025+(j+1));

			GreyContrast[x][y]++;
		}
	}
	for(int i = 0; i < 256; i++){
		for(int j = 0; j < 256; j++){
			calc = GreyContrast[i][j];
			contraste[2] += fabs(i - j)*(calc*calc);
			energia[2] += (calc*calc);
			homogeneidade[2] += calc/(fabs(i - j)+1);
		}
	}
	free(GreyContrast);

	GreyContrast = calloc(256, sizeof(int *)); 
	for(int i = 0; i < 256; i++){
		GreyContrast[i] = calloc(256, sizeof(int));
	}


	for(int i = 1024; i > 0; i--){
		for(int j = 0; j < 1024; j++){
			x = *(matrix+i*1025+j);
			y = *(matrix+(i-1)*1025+(j+1));

			GreyContrast[x][y]++;
		}
	}
	for(int i = 0; i < 256; i++){
		for(int j = 0; j < 256; j++){
			calc = GreyContrast[i][j];
			contraste[3] += fabs(i - j)*(calc*calc);
			energia[3] += (calc*calc);
			homogeneidade[3] += calc/(fabs(i - j)+1);
		}
	}

	for(int i = 0; i < 24; i+=6){
		*(Padrao+512+i) = contraste[k];
		*(Padrao+513+i) = energia[k];
		*(Padrao+514+i) = homogeneidade[k];
		*(Padrao+515+i) = contraste[k];
		*(Padrao+516+i) = energia[k];
		*(Padrao+517+i) = homogeneidade[k];
		k++;
	}
	Normaliza(Padrao);
}